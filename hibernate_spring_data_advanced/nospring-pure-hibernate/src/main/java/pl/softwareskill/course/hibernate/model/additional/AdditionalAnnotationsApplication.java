package pl.softwareskill.course.hibernate.model.additional;

import java.math.BigDecimal;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie anotacji dodatkowych Transient, Immutable, DynamicUpdate
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class AdditionalAnnotationsApplication {

    public static void main(String[] args) {
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        var card = session.get(CardImmutable.class, "1");
        session.remove(card);//Można usunąć encję Immutable
        session.flush();

        var card4 = session.get(CardImmutable.class, "4");
        //Mimo że wywołujesz set dla encji Immutable zignorowane zostaną zmiany i update nie zostanie
        //wyslany do bazy przy flush
        card4.setBalance(card.getBalance().add(BigDecimal.ONE));
        session.flush();//Nie będzie aktualizacji

        //Dynamic update wysyła tylko zmienione kolumny - uwaga na updateable i insertable z anotacji @Column
        var cardDynamicUpdate = session.get(CardDynamicUpdate.class, "3");
        cardDynamicUpdate.setBalance(cardDynamicUpdate.balance.add(BigDecimal.ONE));
        session.flush();

        tx.rollback();

        session.close();
        sessionFactory.close();
    }
}
