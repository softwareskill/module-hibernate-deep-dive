package pl.softwareskill.course.hibernate.model.nplus1;

import com.p6spy.engine.common.StatementInformation;
import com.p6spy.engine.event.JdbcEventListener;
import com.p6spy.engine.logging.LoggingEventListener;
import com.p6spy.engine.spy.JdbcEventListenerFactory;
import com.p6spy.engine.spy.P6SpyDriver;
import static java.util.Objects.isNull;
import java.util.concurrent.atomic.AtomicInteger;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;
import pl.softwareskill.course.hibernate.model.relations.CardEagerUnidirectional;

/**
 * Kod demonstracyjny
 *
 * Aplikacja pokazująca w jaki sposób można przetestować czy liczba spodziewanych zapytań
 * typu SELECT nie przekracza założeń - czyli czy nie wystąpił problem N+1.
 *
 * Natywny Hibernate z wykorzystaniem właściowści sterownika JDBC typu Proxy - P6Spy
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class NPlus1DetectionExample1Application {

    public static void main(String[] args) {

        //Należy zainicjować driver P6 przed inicjalizacją Hibernate
        AtomicInteger sqlSelectCounter = new AtomicInteger();
        //Robimy to w testach - to nie powinien być kod produkcyjny
        initializeSelectQueryListener(sqlSelectCounter);

        //Natywny hibernayte -zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        var before = sqlSelectCounter.get();
        var card = session.get(CardEagerUnidirectional.class, "1");
        var after = sqlSelectCounter.get();
        if (after - before > 1) {
            log.error("Wystąpił problem N+1");
        }

        before = after;
        //Problem N+1
        var cardsList = session.createQuery("from CardEagerUnidirectional", CardEagerUnidirectional.class).getResultList();

        after = sqlSelectCounter.get();
        if (after - before > 1) {
            log.error("Wystąpił problem N+1");
        }

        tx.rollback();

        session.close();
        sessionFactory.close();
    }

    private static void initializeSelectQueryListener(AtomicInteger sqlSelectCounter) {
        //Dedykowany listener dla sterownika P6Spy
        P6SpyDriver.setJdbcEventListenerFactory(new JdbcEventListenerFactory() {
            @Override
            public JdbcEventListener createJdbcEventListener() {
                return new LoggingEventListener() {
                    @Override
                    public void onBeforeAnyExecute(StatementInformation statementInformation) {
                        super.onBeforeAnyExecute(statementInformation);
                        if (isSelect(statementInformation)) {
                            sqlSelectCounter.incrementAndGet();
                        }
                    }

                    private boolean isSelect(StatementInformation statementInformation) {
                        var sql = statementInformation.getSql();
                        if (isNull(sql)) {
                            return false;
                        }
                        return sql.toLowerCase().startsWith("select ");
                    }
                };
            }
        });
    }
}
