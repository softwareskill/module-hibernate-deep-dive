package pl.softwareskill.course.hibernate.model.nplus1;

import java.util.UUID;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;
import pl.softwareskill.course.hibernate.model.relations.CardEagerUnidirectional;

/**
 * Kod demonstracyjny
 *
 * Aplikacja pokazująca w jaki sposób można przetestować czy liczba spodziewanych zapytań
 * typu SELECT nie przekracza założeń - czyli czy nie wystąpił problem N+1.
 *
 * Natywny Hibernate z wykorzystaniem wstrzykiwania identyfikatora operacji do kontekstu loggerów (Logback)
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class NPlus1DetectionExample2Application {

    public static final String OPERATION_ID_MDC_PARAM = "operationId";

    public static void main(String[] args) {

        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        setCurrentOperationIdContext();

        var card = session.get(CardEagerUnidirectional.class, "1");

        stopCurrentOperationIdContext();

        setCurrentOperationIdContext();
        //Problem N+1
        var cardsList = session.createQuery("from CardEagerUnidirectional", CardEagerUnidirectional.class).getResultList();

        setCurrentOperationIdContext();

        tx.rollback();

        session.close();
        sessionFactory.close();
    }

    private static void setCurrentOperationIdContext() {
        final UUID uuid = UUID.randomUUID();
        MDC.put(OPERATION_ID_MDC_PARAM, uuid.toString());
    }

    private static void stopCurrentOperationIdContext() {
        MDC.remove(OPERATION_ID_MDC_PARAM);
    }
}
