package pl.softwareskill.course.hibernate.model;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateInitializer {

    public static SessionFactory initialize() {
        try {
            return new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            throw new DatabaseOperationException(ex);
        }
    }
}
