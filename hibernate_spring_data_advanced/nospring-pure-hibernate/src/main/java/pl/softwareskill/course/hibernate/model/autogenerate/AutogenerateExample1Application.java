package pl.softwareskill.course.hibernate.model.autogenerate;

import java.math.BigDecimal;
import java.util.UUID;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.CardCountry;
import pl.softwareskill.course.hibernate.model.CreditCardAutogenerateSequence;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie autogeneracji klucza głównego z wykorzystaniem sekwencji
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class AutogenerateExample1Application {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        CreditCardAutogenerateSequence card = createNewCard();

        //Zobacz jak działa persist/save dla encji transient z autogeneracją
        //wykorzystującą sekwencję. Zobacz jakie zapytanaia się wygenerują do bazy w związku z
        //nadawaniem klucza główwnego.
        //
        //Zobacz także jakie zapytania będą wygenerowane dla kolejnych nowych encji tego samego typu
        //
        //Zmień ustawienia sekwencji po stronie bazy lub inkrement po stronie Hibernate
        session.persist(card);
//        var mergedCard = session.merge(card); //Po merge używaj wyniku merge ('zapomnij' o card podawanej w argumencie merge(
//        session.saveOrUpdate(card);

        //Wysłanie zapytań do bazy danych
        session.flush();

        //Druga karta
        card = createNewCard();

        session.persist(card);
        //Czy zostanie pobrana wartośc z sekwencji?
        session.flush();

        tx.rollback();

        session.close();
        sessionFactory.close();
    }

    private static CreditCardAutogenerateSequence createNewCard() {
        var card = new CreditCardAutogenerateSequence();
        card.setCardUuid(UUID.randomUUID().toString());
        card.setCardOwnerId("1");
        card.setEnabled(false);
        card.setCardCountry(CardCountry.EN);
        card.setBalance(BigDecimal.ONE);
        return card;
    }
}
