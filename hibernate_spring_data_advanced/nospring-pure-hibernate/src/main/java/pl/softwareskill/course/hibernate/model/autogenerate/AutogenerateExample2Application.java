package pl.softwareskill.course.hibernate.model.autogenerate;

import java.math.BigDecimal;
import java.util.UUID;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.CardCountry;
import pl.softwareskill.course.hibernate.model.CreditCardAutogenerateSequence;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie autogeneracji klucza głównego z wykorzystaniem sekwencji
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class AutogenerateExample2Application {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        var card = new CreditCardAutogenerateSequence();
        card.setCardUuid(UUID.randomUUID().toString());
        card.setBalance(BigDecimal.ZERO);
        card.setCardCountry(CardCountry.EN);
        card.setCardOwnerId("1");
        card.setEnabled(false);

        //Zobacz jak działają rózne matiody session dla encji transient z autogeneracją
        //wykorzystującą sekwencję. Zobacz jakie zapytanaia się wygenerują do bazy w związku z
        //nadawaniem klucza główwnego.
        //
        //Zobacz także jakie zapytania będą wygenerowane dla kolejnych nowych encji tego samego typu
        //
        //Zmień ustawienia sekwencji po stronie bazy lub inkrement po stronie Hibernate

        session.persist(card);
        //Sprawdź inne metody niż persist
        //var mergedCard = session.merge(card);
        //session.update(card);
        //session.save(card);
        //session.saveOrUpdate(card);

        //Wysłanie zapytań do bazy danych
        session.flush();

        tx.rollback();

        session.close();
        sessionFactory.close();
    }
}
