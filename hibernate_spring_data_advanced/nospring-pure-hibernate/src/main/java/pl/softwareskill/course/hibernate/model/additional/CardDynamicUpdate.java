package pl.softwareskill.course.hibernate.model.additional;

import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import pl.softwareskill.course.hibernate.model.CardCountry;

@Entity
@DynamicUpdate
@Table(name = "CARDS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardDynamicUpdate {
    @Id
    @Column(name = "CARD_ID")
    String cardId;

    @Column(name = "CARD_UUID")
    String cardUuid;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;

    @Column(scale = 10, precision = 2)
    BigDecimal balance;

    //Data odczytania z bazy danych - nie ma tej danej w bazie
    @Transient
    Instant readDate;
}
