package pl.softwareskill.course.hibernate.model.basic;

import java.math.BigDecimal;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import pl.softwareskill.course.hibernate.model.Card;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;


/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie podstawowych anotacji Hibernate w trybie natywnym
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class BasicAnnotationsExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        //Użyj metod dla sprawdzenia działnia
//        entityAnnotationRun(sessionFactory);
        columnAnnotationRun(sessionFactory);

        sessionFactory.close();
    }

    private static void entityAnnotationRun(SessionFactory sessionFactory) {
        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();
        tx.begin();

        //Sprawdź co jest w PersistentContext,
        //Zobacz jakie zapytania się wygenerują
        var card = session.get(Card.class, "1");

        tx.rollback();

        session.close();
    }

    private static void columnAnnotationRun(SessionFactory sessionFactory) {
        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();
        tx.begin();

        var card = session.get(Card.class, "1");

        //Sprawdź działanie metod Session np. persist czy też zmodyfikuj dane encji
        //Sprawdź jaki wpływ na działanie mają ustawienia anotacji @Column
//        card.setCreatedAt(Calendar.getInstance());
        card.setCardUuid("3333");
        card.setEnabled(!card.getEnabled());
        card.setBalance(new BigDecimal("12345.678"));
        //var newCard = copyCard(card);

        //Sprawdź różne metody Session
        //session.persist(newCard);

        //Wysłanie zapytań do bazy danych
        session.flush();

        tx.commit();

        session.close();
    }

    private static Card copyCard(Card source) {
        var newCard = new Card();
        newCard.setCardId(String.valueOf(System.currentTimeMillis()));

        newCard.setCardUuid(source.getCardUuid());//kopiujemy UUID

        return newCard;
    }
}
