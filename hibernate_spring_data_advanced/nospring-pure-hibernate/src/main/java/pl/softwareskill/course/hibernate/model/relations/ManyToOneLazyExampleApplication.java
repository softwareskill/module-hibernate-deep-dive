package pl.softwareskill.course.hibernate.model.relations;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie anotacji związanych z releacjami
 * oraz dociąganiem typu LAZY
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class ManyToOneLazyExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        var card = session.get(CardLazyUnidirectional.class, "1");

        //Próba pobrania imienia spowoduje doczytanie - czyli select
        card.getCardOwner().getFirstName();

        //Problem N+1 nie wystąpi przy zapytaniu
        var cardsList = session.createQuery(
                "from CardLazyUnidirectional",
                CardLazyUnidirectional.class).getResultList();
        ///ale w momencie doczytywania własności z obiektu
        cardsList.forEach(cardFromList -> cardFromList.getCardOwner().getFirstName());

        tx.rollback();

        session.close();
        sessionFactory.close();
    }
}
