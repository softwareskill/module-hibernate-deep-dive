package pl.softwareskill.course.hibernate.model.relations;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "USERS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserUnidirectional implements Serializable {
    @Id
    @Column(name = "USER_ID")
    String userId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;
}
