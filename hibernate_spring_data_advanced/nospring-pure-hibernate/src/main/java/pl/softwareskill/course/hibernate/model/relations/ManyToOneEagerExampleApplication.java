package pl.softwareskill.course.hibernate.model.relations;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie anotacji związanych z releacjami
 * oraz dociąganiem typu EAGER
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class ManyToOneEagerExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        //Możesz użyć innej encji i/lub zmienić relację na dwukierunkową
        var card = session.get(CardEagerUnidirectional.class, "1");

        //Próba pobrania imienia nie powoduje doczytania bo relacja jest EAGER
        card.getCardOwner().getFirstName();

        //Problem N+1
//        var cardsList = session.createQuery("from CardEagerUnidirectional", CardEagerUnidirectional.class).getResultList();

        //1. Rozwiązanie N+1 - join fetch
        //Dla left join fetch zostanie wysłane jedno zapytanie - left join aby zwrócić karty bez właściela (bez tego będzie inner join)
//        var query = session.createQuery(
//                "from CardEagerUnidirectional c left join fetch c.cardOwner",
//                CardEagerUnidirectional.class);
//        var cardsList = query.getResultList();

        //2. Rozwiązanie - napełnienie PC danymi o właścicielach
        session.createQuery("from UserUnidirectional", UserUnidirectional.class).getResultList();
        var cardsList = session.createQuery("from CardEagerUnidirectional", CardEagerUnidirectional.class).getResultList();

//        session.refresh(card);

        tx.rollback();

        session.close();
        sessionFactory.close();
    }
}
