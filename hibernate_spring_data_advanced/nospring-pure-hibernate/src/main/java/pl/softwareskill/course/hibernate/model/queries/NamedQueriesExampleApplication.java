package pl.softwareskill.course.hibernate.model.queries;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.CardCountry;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;

@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class NamedQueriesExampleApplication {

    public static void main(String[] args) {
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        var query = session.createNamedQuery("findByCountry", CardWithQueries.class);
        query.setParameter("country", CardCountry.DE);

        //zobaczmy co w PersistenceContext
        var foundCards = query.getResultList();

        var nativeQuery = session.getNamedNativeQuery("findByCountryNative");
        nativeQuery.setParameter(1, CardCountry.DE.name());//Nie zadziała mapowanie enuma musi być wartość

        //zobaczmy co w PersistenceContext
        var foundCardsFromNative = nativeQuery.getResultList();

        tx.rollback();

        session.close();
        sessionFactory.close();
    }
}
