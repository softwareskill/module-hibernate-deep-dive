package pl.softwareskill.course.hibernate.model.queries;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.softwareskill.course.hibernate.model.CardCountry;

@Entity
@Table(name = "CARDS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries(
        @NamedQuery(name = "findByCountry", query = "from CardWithQueries where cardCountry=:country")
)
@NamedNativeQueries(
        @NamedNativeQuery(name = "findByCountryNative",
                query = "select CARD_ID,CARD_UUID,COUNTRY,BALANCE from CARDS where COUNTRY=?",
                resultClass = CardWithQueries.class)
)
public class CardWithQueries {
    @Id
    @Column(name = "CARD_ID")
    String cardId;

    @Column(name = "CARD_UUID")
    String cardUuid;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;

    @Column(scale = 10, precision = 2)
    BigDecimal balance;
}
