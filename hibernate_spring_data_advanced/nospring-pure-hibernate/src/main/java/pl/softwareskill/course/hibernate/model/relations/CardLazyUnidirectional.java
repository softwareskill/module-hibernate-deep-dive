package pl.softwareskill.course.hibernate.model.relations;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.softwareskill.course.hibernate.model.CardCountry;

@Entity
@Table(name = "CARDS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
//Jeżeli nie będzie exclude Intellij może wywołać doczytanie cardOwner
@ToString(exclude = "cardOwner")
public class CardLazyUnidirectional {
    @Id
    @Column(name = "CARD_ID")
    String cardId;

    @Column(name = "CARD_UUID")
    String cardUuid;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CARD_OWNER_ID")
    UserUnidirectional cardOwner;

    @Column(scale = 10, precision = 2)
    BigDecimal balance;
}
