package pl.softwareskill.course.hibernate.model.inheritance;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;
import pl.softwareskill.course.hibernate.model.RealEstate;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie anotacji Embedded i Embeddable
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class EmbeddedExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        var realEstate = session.get(RealEstate.class, 1L);

        tx.rollback();

        session.close();
        sessionFactory.close();
    }
}
