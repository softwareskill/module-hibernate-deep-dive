package pl.softwareskill.course.hibernate.model.inheritance;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import pl.softwareskill.course.hibernate.model.Car;
import pl.softwareskill.course.hibernate.model.HibernateInitializer;
import pl.softwareskill.course.hibernate.model.Vehicle;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje działanie anotacji związanych z dziedziczeniem Inheritance/DiscriminatorColumn/DiscriminatorValue
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class SingleTableInheritanceExampleApplication {

    public static void main(String[] args) {
        //Inicjalizacja Hibernate zobacz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();
        var tx = session.getTransaction();

        tx.begin();

        var vehicles = session.createQuery("FROM Vehicle", Vehicle.class).getResultList();

        var cars = session.createQuery("FROM Car", Car.class).getResultList();

        tx.rollback();

        session.close();
        sessionFactory.close();
    }
}
