package pl.softwareskill.course.hibernate.model;

public class DatabaseOperationException extends RuntimeException {
    public DatabaseOperationException(Throwable cause) {
        super(cause);
    }
}
