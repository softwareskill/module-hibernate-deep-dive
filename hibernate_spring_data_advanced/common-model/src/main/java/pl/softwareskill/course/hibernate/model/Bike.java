package pl.softwareskill.course.hibernate.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@DiscriminatorValue("bike")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
class Bike extends Vehicle {

    @Column(name = "FRONT_COUNT")
    int frontDerailleurCount;

    @Column(name = "REAR_COUNT")
    int rearDerailleurCount;
}
