package pl.softwareskill.course.hibernate.model;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "CARDS",
        schema = "SOFTWARESKILL",
        //Wykorzystywane podczas automatycznego tworzenia tabel
        uniqueConstraints = {
                @UniqueConstraint(name = "UQ_CARD_UUID", columnNames = {"CARD_UUID"})
        },
        //Wykorzystywane podczas automatycznego tworzenia tabel
        indexes = {
                @Index(name = "UQ_CARD_UUID", unique = true, columnList = "CARD_UUID")
        }
)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "cardOwner")
public class Card {

    @Id
    @Column(name = "CARD_ID")
    String cardId;

    @Column(name = "CARD_UUID",
            //Walidacje
            length = 40, //Długość dla pola tekstowego,
            updatable = false,
            unique = true) //Unikalność wartości
    String cardUuid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CARD_OWNER_ID")
    User cardOwner;

    @Column(name = "ENABLED",
            insertable = false, //Czy wartość pobierana do persist?
            updatable = true, //Czy wartość pobierana do aktualizacji?
            //Walidacje
            nullable = false, //Czy dopuszczalna wartość null
            length = 1)//Długość dla pola tekstowego
    @Convert(converter = YesNoBooleanConverter.class)
    Boolean enabled;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;

    //Nie podajemy nazwy kolumny gdyż jest taka sama jak nazwa pola
    @Column(
            //Walidacje
            scale = 3, precision = 2
    )
    BigDecimal balance;

    @Column(name = "EXPIRES_AT")
    @Temporal(TemporalType.DATE)
    Calendar expiresAt;

    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.DATE)
    Calendar createdAt;

    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.DATE)
    Calendar updatedAt;

    @Column(name = "PNG_IMAGE")
    @Lob
    @Type(type = "org.hibernate.type.BinaryType")//Mapowanie wymagane dla PostgreSQL
    byte[] pngImage;

//    @PreUpdate
//    void markUpdatedNow() {
//        this.updatedAt = Calendar.getInstance();
//    }
}
