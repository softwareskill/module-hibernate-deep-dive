package pl.softwareskill.course.hibernate.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@DiscriminatorValue("car")
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class Car extends Vehicle {

    @Column(name = "GEAR_COUNT")
    int gearCount;

    @Column(name = "CAR_BRAND")
    String carBrand;
}
