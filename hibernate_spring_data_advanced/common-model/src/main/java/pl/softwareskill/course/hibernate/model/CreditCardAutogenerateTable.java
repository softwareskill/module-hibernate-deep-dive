package pl.softwareskill.course.hibernate.model;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "CREDIT_CARDS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreditCardAutogenerateTable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "AUTOGENERATE")
    @TableGenerator(name = "AUTOGENERATE", table = "TABLE_AUTOGENERATE", schema = "SOFTWARESKILL")
    @Column(name = "CARD_ID")
    Long cardId;

    @Column(name = "CARD_UUID")
    String cardUuid;

    @Column(name = "CARD_OWNER_ID")
    String cardOwnerId;

    @Column(name = "ENABLED")
    @Convert(converter = YesNoBooleanConverter.class)
    Boolean enabled;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    CardCountry cardCountry;

    //Nie podajemy nazwy kolumny gdyż jest taka sama jak nazwa pola
    BigDecimal balance;

    @Column(name = "EXPIRES_AT")
    @Temporal(TemporalType.DATE)
    Calendar expiresAt;

    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    @CreationTimestamp
    Calendar createdAt;

    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    @UpdateTimestamp
    Calendar updatedAt;
}
