package pl.softwareskill.course.hibernate.model;

public enum CardCountry {
    PL,
    EN,
    DE,
    EA
}
