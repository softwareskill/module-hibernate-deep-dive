package pl.softwareskill.course.hibernate.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
@ToString
@Getter
@Setter
abstract class AuditableEntity {

    @Column(name = "CREATED_AT")
    @CreationTimestamp
    LocalDateTime createdAt;

    @Column(name = "UPDATED_AT")
    @UpdateTimestamp
    LocalDateTime updatedAt;
}
