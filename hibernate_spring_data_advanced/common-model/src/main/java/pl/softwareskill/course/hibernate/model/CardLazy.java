package pl.softwareskill.course.hibernate.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Proxy
@Table(name = "CARDS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardLazy implements Serializable {
    @Id
    @Column(name = "CARD_ID")
    private String cardId;

    @Column(name = "CARD_UUID")
    @Basic(fetch = FetchType.LAZY, optional = false)
    private String cardUuid;

    @Column(name = "COUNTRY")
    @Enumerated(EnumType.STRING)
    @Basic(fetch = FetchType.LAZY, optional = false)
    private CardCountry cardCountry;

    @Column(scale = 10, precision = 2)
    private BigDecimal balance;

    @Column(name = "EXPIRES_AT")
    @Temporal(TemporalType.DATE)
    private Calendar expiresAt;

    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar createdAt;

    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.TIMESTAMP)
    @Basic(optional = true)
    @UpdateTimestamp
    private Calendar updatedAt;

    @PreUpdate
    void markUpdatedNow() {
        this.updatedAt = Calendar.getInstance();
    }
}
