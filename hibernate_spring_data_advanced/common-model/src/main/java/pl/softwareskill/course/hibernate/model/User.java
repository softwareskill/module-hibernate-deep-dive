package pl.softwareskill.course.hibernate.model;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.Formula;

@Entity
@Table(name = "USERS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "userCards")
public class User {
    @Id
    @Column(name = "USER_ID")
    String userId;

    @Column(name = "FIRST_NAME")
    String firstName;

    @Column(name = "LAST_NAME")
    String lastName;

    //Definicja dla relacji dwukierunkowej
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}, orphanRemoval = true)
    @JoinColumn(name = "CARD_OWNER_ID")
    List<Card> userCards;

    @Formula("(SELECT COUNT(1) FROM SOFTWARESKILL.CARDS C WHERE C.CARD_OWNER_ID=USER_ID)")
    int cardsCount;

    @ColumnTransformer(forColumn = "PASSWORD",
            read = "HEXTORAW(PASSWORD)",
            write = "RAWTOHEX(PASSWORD)")
    String password;
}
