package pl.softwareskill.course.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import lombok.Data;
import lombok.ToString;

@Entity
@Table(name = "REAL_ESTATES", schema = "SOFTWARESKILL")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@ToString
public class RealEstate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REAL_ESTATES_SEQUENCE")
    @Column(name = "ID")
    Long id;

    @Embedded
    Address address;//Address nie jest encją, nie musi być rejestrowany w konfiguracji
}
