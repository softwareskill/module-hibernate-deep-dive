package pl.softwareskill.course.hibernate.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "DEPARTMENTS", schema = "SOFTWARESKILL")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "manager")
public class Department {
    @Id
    @Column(name = "DEPARTMENT_ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DEPARTMENTS")
    Long departmentId;

    String name;

    @Embedded
    Address address;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "MANAGER_ID")
    Manager manager;
}
