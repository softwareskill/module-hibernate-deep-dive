package pl.softwareskill.course.hibernate.cascade;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.hibernate.DepartmentRepository;
import pl.softwareskill.course.hibernate.ManagerRepository;
import pl.softwareskill.course.hibernate.OperationResult;
import pl.softwareskill.course.hibernate.model.Address;
import pl.softwareskill.course.hibernate.model.Department;
import pl.softwareskill.course.hibernate.model.Manager;
import pl.softwareskill.course.hibernate.relations.UserRepository;


/**
 * Kod demonstracyjny
 *
 * Kontroler z endpointami dla pokazania działania kaskady operacji i usuwania osieroconych encji
 *
 * Aplikacja SpringBoot
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@Transactional
@FieldDefaults(makeFinal = true, level = PRIVATE)
@SuppressWarnings("unused")
public class OperationCascadeController {

    ManagerRepository managerRepository;
    DepartmentRepository departmentRepository;
    UserRepository userRepository;

    @PersistenceContext
    EntityManager entityManager;

    @PostMapping("cascade/departments")
    public OperationResult cascadeNewDepartment() {
        //Próba dodania nowego departamentu
        Department newDepartment = createNewDepartment(true);

        //Bez kaskady PERSIST zapis się nie powiedzie jeśli relacja jest wymagana a encja jest transient
        //Jeśli nie chcesz używać kaskady to zapisz obiekt z relacji wcześniej przez odpowiednie DAO
        departmentRepository.save(newDepartment);

        return new OperationResult(String.valueOf(newDepartment.getDepartmentId()));
    }

    private static Department createNewDepartment(boolean withNewManager) {
        var newDepartment = new Department();
        var address = new Address();
        address.setCountry("PL");
        address.setCity("Gniezno");
        address.setStreet("Prosta");
        address.setNumber("1");
        address.setPostalCode("34-091");
        newDepartment.setAddress(address);
        newDepartment.setName("Szkolenie");
        if (withNewManager) {
            var manager = new Manager();
            manager.setFirstName("FirstName" + System.currentTimeMillis());
            manager.setLastName("LastName" + System.currentTimeMillis());
            // Dla relacji dwukierunkowej ustawienie departamentu
            manager.setDepartment(newDepartment);
            newDepartment.setManager(manager);
        }
        return newDepartment;
    }

    @PutMapping("cascade/departments/{id}")
    public OperationResult cascadeEdit(@PathVariable Long id) {

        var departmentOptional = departmentRepository.findById(id);

        departmentOptional.ifPresent(department -> {
            department.setName("AAAA" + System.currentTimeMillis());
            department.getManager().setFirstName("AAAA" + System.currentTimeMillis());

        });

        return new OperationResult("OK");
    }

    @DeleteMapping("cascade/users/{id}")
    public OperationResult deleteUser(@PathVariable String id) {
        userRepository.findById(id)
                .ifPresent(userRepository::delete);

        entityManager.flush();

        throw new RuntimeException("Plantain wyjatek dla rollback");
    }

    @DeleteMapping("cascade/departments/{id}")
    public OperationResult cascadeDelete(@PathVariable Long id) {

        departmentRepository.deleteById(id);

        return new OperationResult("OK");
    }

    @PutMapping("cascade/departments/{id}/clearManager")
    public OperationResult cascadeClearManagerWithOrphanRemoval(@PathVariable Long id) {

        var departmentOptional = departmentRepository.findById(id);

        departmentOptional.ifPresent(department -> department.setManager(null));

        return new OperationResult("OK");
    }

    @DeleteMapping("cascade/users/{id}/removeFirstCard")
    public OperationResult cascadeDeleteCardWithOrphanRemoval(@PathVariable String id) {

        var userOptional = userRepository.findById(id);
        //Ustawiając null z włączonym orphanRemoval zostanie usunięty obiekt
        userOptional.ifPresent(user -> {
            var cards = user.getUserCards();
            cards.stream()
                    .filter(card -> "1".equals(card.getCardUuid()))
                    .findFirst()
                    .ifPresent(card -> {
                        //Usuwanie osieroconej encji
                        //Jeśli relacja dwukierunkowa i nie będzie czyszczenia nie wystąpi orphan removal
                        //card.setCardOwner(null);
                        //cards.remove(card);

                        //Remove nie usunie encji z relacji
                        entityManager.remove(card);
                    });
        });

        entityManager.flush();

        throw new RuntimeException("Wymuszenie rollback");
    }

    @PutMapping("cascade/departments/{id}/merge")
    //Musi być @RequestBody inaczej nie nastąpi mapowanie
    public OperationResult cascadeMerge(@PathVariable Long id, @RequestBody Department department) {

        //Jeśli manage przyjdzie jako null i będzie orphanRemoval wówczas zostanie usunięta encja powiązana
        departmentRepository.save(department);

        return new OperationResult("OK");
    }

    @PutMapping("cascade/departments/{id}/evictRefresh")
    public OperationResult cascadeEvictRefresh(@PathVariable Long id) {

        var departmentOptional = departmentRepository.findById(id);

        departmentOptional.ifPresent(entityManager::refresh);

        return new OperationResult("OK");
    }
}
