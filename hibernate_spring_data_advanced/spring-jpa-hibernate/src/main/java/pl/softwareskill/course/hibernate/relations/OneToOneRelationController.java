package pl.softwareskill.course.hibernate.relations;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.hibernate.DepartmentRepository;
import pl.softwareskill.course.hibernate.ManagerRepository;
import pl.softwareskill.course.hibernate.OperationResult;
import pl.softwareskill.course.hibernate.model.Address;
import pl.softwareskill.course.hibernate.model.Department;
import pl.softwareskill.course.hibernate.model.Manager;

/**
 * Kod demonstracyjny
 *
 * Kontroler z endpointami dla pokazania relacji OneToOne
 *
 * Aplikacja SpringBoot
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@Transactional //Bez transactional nie zadziała modyfikacja danych
@FieldDefaults(makeFinal = true, level = PRIVATE)
@SuppressWarnings("unused")
public class OneToOneRelationController {

    ManagerRepository managerRepository;
    DepartmentRepository departmentRepository;

    @PersistenceContext
    EntityManager entityManager;

    @GetMapping("one2one/departments/{id}")
    ResponseEntity<?> readDepartment(@PathVariable Long id) {
        log.info("Przed odczytem");
        var department = departmentRepository.findById(id).get();
        log.info("Po odczycie");

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("one2one/managers/{id}")
    ResponseEntity<?> readManager(@PathVariable Long id) {
        log.info("Przed odczytem");
        var manager = managerRepository.findById(id).get();
        log.info("Po odczycie");

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    //Bez public nie zadziałają automatyczne transakcje w Spring
    @PutMapping("one2one/departments/{id}/clearManager")
    public ResponseEntity<?> clearManager(@PathVariable Long id) {
        log.info("Przed odczytem");
        var department = departmentRepository.findById(id).get();
        log.info("Po odczycie");
        department.setManager(null);

        entityManager.flush();

        throw new RuntimeException("Celowy wyjątek");
    }

    //Bez public nie zadziałają automatyczne transakcje w Spring
    @PutMapping("one2one/managers")
    public ResponseEntity<?> createManager() {
        var manager = new Manager();
        manager.setFirstName("FFFF");
        manager.setLastName("FFFF");

        var department = new Department();
        department.setName("Nowy");

        var address = new Address();

        address.setCountry("PL");
        address.setCity("Gniezno");
        address.setStreet("Prosta");
        address.setNumber("1");
        address.setPostalCode("34-091");
        department.setAddress(address);

        department.setManager(manager);
//        manager.setDepartment(department);

//        managerRepository.save(manager);
        departmentRepository.save(department);

        entityManager.flush();

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("one2one/departments/noNPlus1")
    ResponseEntity<?> readAllDepartmentsNoNPlus1() {

        //Bez N+1 - złączenie JOIN
        var departmentsListNoNPlus1 = entityManager.createQuery(
                "from Department d left join fetch d.manager",
                Department.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("one2one/departments/nPlus1")
    ResponseEntity<?> readAllDepartmentsNPlus1() {

        //N+1
        var departmentsListNPlus1 = entityManager.createQuery(
                "from Department",
                Department.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }
}
