package pl.softwareskill.course.hibernate.relations;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.hibernate.model.Card;

@Repository
public interface CardRepository extends CrudRepository<Card, String> {
}
