package pl.softwareskill.course.hibernate.relations;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.hibernate.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, String> {
}
