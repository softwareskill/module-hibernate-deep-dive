package pl.softwareskill.course.hibernate;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.hibernate.model.Manager;

@Repository
public interface ManagerRepository extends CrudRepository<Manager, Long> {
}
