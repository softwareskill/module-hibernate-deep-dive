package pl.softwareskill.course.hibernate.relations;

import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.hibernate.OperationResult;
import pl.softwareskill.course.hibernate.model.Card;

/**
 * Kod demonstracyjny
 *
 * Kontroler z endpointami dla pokazania relacji ManyToOne
 *
 * Aplikacja SpringBoot
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@Transactional //Jeżeli nie ma modyfikacji nie musi być Transactional i publicznych metod
@FieldDefaults(makeFinal = true, level = PRIVATE)
@SuppressWarnings("unused")
public class ManyToOneRelationController {

    UserRepository userRepository;
    CardRepository cardRepository;

    @PersistenceContext
    EntityManager entityManager;

    @GetMapping("many2one/cards/{id}")
    public ResponseEntity<?> readCard(@PathVariable String id) {
        log.info("Przed odczytem");
        var card = cardRepository.findById(id).get();
        log.info("Po odczycie");

        card.setBalance(card.getBalance().add(BigDecimal.ONE));
        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("many2one/users/{id}")
    ResponseEntity<?> readUser(@PathVariable String id) {
        log.info("Przed odczytem");
        var user = userRepository.findById(id).get();
        log.info("Po odczycie");

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("many2one/cards/noNPlus1")
    ResponseEntity<?> readAllCardsNoNPlus1() {

        //Bez N+1 - złączenie JOIN
        var cardsListNoNPlus1 = entityManager.createQuery(
                "from Card c left join fetch c.cardOwner",
                Card.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("many2one/cards/nPlus1")
    ResponseEntity<?> readAllCardsNPlus1() {

        //N+1
        var cardsListNPlus1 = entityManager.createQuery(
                "from Card",
                Card.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }
}
