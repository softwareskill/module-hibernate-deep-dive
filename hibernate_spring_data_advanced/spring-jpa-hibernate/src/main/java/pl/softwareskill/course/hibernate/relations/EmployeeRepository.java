package pl.softwareskill.course.hibernate.relations;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.softwareskill.course.hibernate.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
