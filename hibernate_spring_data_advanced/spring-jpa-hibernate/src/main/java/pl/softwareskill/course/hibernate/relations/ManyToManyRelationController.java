package pl.softwareskill.course.hibernate.relations;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.hibernate.OperationResult;
import pl.softwareskill.course.hibernate.model.Company;

/**
 * Kod demonstracyjny
 *
 * Kontroler z endpointami dla pokazania relacji ManyToMany
 *
 * Aplikacja SpringBoot
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@Transactional //Jeżeli nie ma modyfikacji nie musi być Transactional i publicznych metod
@FieldDefaults(makeFinal = true, level = PRIVATE)
@SuppressWarnings("unused")
public class ManyToManyRelationController {

    CompanyRepository companyRepository;
    EmployeeRepository employeeRepository;

    @PersistenceContext
    EntityManager entityManager;

    @GetMapping("many2many/companies/{id}")
    ResponseEntity<?> readCompany(@PathVariable Long id) {
        log.info("Przed odczytem");
        var company = companyRepository.findById(id).get();
        log.info("Po odczycie");

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("many2many/employees/{id}")
    ResponseEntity<?> readEmployee(@PathVariable Long id) {
        log.info("Przed odczytem");
        var employee = employeeRepository.findById(id).get();
        log.info("Po odczycie");

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("many2many/companies/noNPlus1")
    ResponseEntity<?> readAllCompaniesNoNPlus1() {

        //Bez N+1 - złączenie JOIN
        var companiesListNoNPlus1 = entityManager.createQuery(
                "from Company c left join fetch c.employees",
                Company.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("many2many/companies/nPlus1")
    ResponseEntity<?> readAllCompaniesNPlus1() {

        //N+1
        var companiesListNPlus1 = entityManager.createQuery(
                "from Company",
                Company.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }
}
