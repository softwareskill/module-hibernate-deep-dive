package pl.softwareskill.course.hibernate.relations;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.hibernate.OperationResult;
import pl.softwareskill.course.hibernate.model.User;

/**
 * Kod demonstracyjny
 *
 * Kontroler z endpointami dla pokazania relacji OneToMany
 *
 * Aplikacja SpringBoot
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@Transactional //Jeżeli nie ma modyfikacji nie musi być Transactional i publicznych metod
@FieldDefaults(makeFinal = true, level = PRIVATE)
@SuppressWarnings("unused")
public class OneToManyRelationController {

    UserRepository userRepository;
    CardRepository cardRepository;

    @PersistenceContext
    EntityManager entityManager;

    @GetMapping("one2many/users/{id}")
    ResponseEntity<?> readUser(@PathVariable String id) {
        log.info("Przed odczytem");
        var user = userRepository.findById(id).get();
        log.info("Po odczycie");

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("one2many/users/noNPlus1")
    ResponseEntity<?> readAllUsersNoNPlus1() {

        //Bez N+1 - złączenie JOIN
        var usersListNoNPlus1 = entityManager.createQuery(
                "from User u left join fetch u.userCards",
                User.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }

    @GetMapping("one2many/users/nPlus1")
    ResponseEntity<?> readAllUsersNPlus1() {

        //N+1
        var usersListNPlus1 = entityManager.createQuery(
                "from User",
                User.class).getResultList();

        return ResponseEntity.ok(new OperationResult("OK"));
    }
}
