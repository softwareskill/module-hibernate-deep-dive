package pl.softwareskill.course.hibernate.persistencecontext;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import pl.softwareskill.course.hibernate.DatabaseOperationException;

public class JpaHibernateInitializer {

    public static EntityManager initialize() {
        try {
            var entityManagerFactory = Persistence.createEntityManagerFactory("SoftwareSkill");
            return entityManagerFactory.createEntityManager();
        } catch (Throwable ex) {
            throw new DatabaseOperationException(ex);
        }
    }
}
