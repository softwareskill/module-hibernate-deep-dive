package pl.softwareskill.course.hibernate.persistencecontext;

import javax.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import pl.softwareskill.course.hibernate.Card;

@RequiredArgsConstructor
class JPAPersistenceContextCardRepository {

    private final EntityManager entityManager;

    public Card findById(String cardId) {

        var tx = entityManager.getTransaction();
        tx.begin();

        Card card = entityManager.find(Card.class, cardId);

        tx.commit();//rollback i commit NIE czyści PC

        card = entityManager.find(Card.class, cardId); //find po rollback/commit nie rzuci wyjątku

        return card;
    }
}