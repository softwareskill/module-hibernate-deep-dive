package pl.softwareskill.course.hibernate.persistencecontext;

import static lombok.AccessLevel.PRIVATE;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pl.softwareskill.course.hibernate.User;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pokazuje wykorzystanie Hibernate w trybie JPA w Spring z użyciem
 * własnych implementacji Dao/Repository
 *
 * Uruchomienie nie wymaga podania parametrów
 */
@SpringBootApplication
@EnableTransactionManagement //Konfiguracja - włączenie transakcji
@EntityScan(basePackageClasses = User.class)
@FieldDefaults(makeFinal = true, level = PRIVATE)
@RequiredArgsConstructor
@Slf4j
public class SpringWithHibernateJpaApplication implements CommandLineRunner {

    HibernateWithJpaUserRepository repository;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder()
                .sources(SpringWithHibernateJpaApplication.class)
                .web(WebApplicationType.NONE)
                .build();
        app.run(args).close();
    }

    @Override
    public void run(String... args) {

        //Wyszukanie danych
        var user = repository.findById("1"); //Nie potrzeba transakcji dla odczytu danych w trybie JPA
    }
}
