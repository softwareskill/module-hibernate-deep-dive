package pl.softwareskill.course.hibernate;

import static java.util.Objects.isNull;
import javax.persistence.AttributeConverter;

public class YesNoBooleanConverter implements AttributeConverter<Boolean, String> {

    @Override
    public String convertToDatabaseColumn(Boolean attribute) {
        if (isNull(attribute)) {
            return null;
        }
        if (attribute) return "Y";
        return "N";
    }

    @Override
    public Boolean convertToEntityAttribute(String dbData) {
        if (isNull(dbData)) {
            return null;
        } else if ("Y".equals(dbData)) {
            return Boolean.TRUE;
        } else if ("N".equals(dbData)) {
            return Boolean.FALSE;
        }
        throw new DatabaseOperationException(new IllegalStateException("Nieprawidłowa wartość - dopuszczalne Y i N"));
    }
}
