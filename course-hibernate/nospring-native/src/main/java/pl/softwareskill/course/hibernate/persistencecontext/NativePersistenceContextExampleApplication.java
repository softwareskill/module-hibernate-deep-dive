package pl.softwareskill.course.hibernate.persistencecontext;

import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa. Pokazuje proces napełniania PersistenceContext dla dwóch rodzajów encji (weryfikacja
 * w trybie debug).
 * W zależności od tego czy relacja jest LAZY czy EAGER i typu relacji w PC będą zarejestrowane różne obiekty.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativePersistenceContextExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var cardRepository = new NativePersistenceContextCardRepository(session);
        //Możesz sprawdzić dla konfiguracji LAZY i EAGER dla relacji
        var card = cardRepository.findById("1");

        var userRepository = new NativePersistenceContextUserRepository(session);
        //Możesz sprawdzić dla konfiguracji LAZY i EAGER dla relacji
        var user = userRepository.findById("1");

        userRepository.findById("1");
        //close resources
        session.close();
        sessionFactory.close();
    }

}
