package pl.softwareskill.course.hibernate.persistencecontext;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import pl.softwareskill.course.hibernate.User;

@RequiredArgsConstructor
class NativePersistenceContextUserRepository {

    private final Session session;

    public User findById(String userId) {

        var tx = session.getTransaction();
        if (!tx.isActive()) {
            tx.setTimeout(1);//timeout ustawiany w sekundach
            tx.begin();
        }

        User user = session.get(User.class, userId);

        return user;
    }
}