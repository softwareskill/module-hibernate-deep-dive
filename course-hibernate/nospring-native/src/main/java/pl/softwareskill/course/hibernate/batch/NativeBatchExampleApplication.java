package pl.softwareskill.course.hibernate.batch;

import java.util.concurrent.atomic.AtomicInteger;
import pl.softwareskill.course.hibernate.Card;
import pl.softwareskill.course.hibernate.HibernateInitializer;

/**
 * Kod demonstracyjny
 *
 * Aplikacja konsolowa - prezentuje przetwarzanie batchowe w Hibernate (grupowanie zmian na encji w paczki).
 * Rozmiar batch sterowany przez property hibernate.jdbc.batch_size.
 *
 * Log level dla org.hibernate.engine.jdbc.batch.internal.BatchingBatch musi być równy debug aby zobaczyć wpisy
 *
 * Logika pobiera wszystkie karty, następnie zmienia dane wszystkich kart poza kartami o indeksie 11-19,
 * gdyż te usuwa.
 *
 * Uruchomienie nie wymaga podania parametrów
 */
public class NativeBatchExampleApplication {

    public static void main(String[] args) {
        //inicjalizacja Hibernate - zobacz hibernate.properties oraz hibernate.cfg.xml
        var sessionFactory = HibernateInitializer.initialize();

        var session = sessionFactory.getCurrentSession();

        var tx = session.beginTransaction();

        var query = session.createQuery("FROM Card", Card.class);
        var cardsList = query.getResultList();

        var index = new AtomicInteger();
        cardsList.forEach(card -> {
            System.out.println("Index = " + index.addAndGet(1));
            card.setEnabled(!card.getEnabled());//Zmiana danych UPDATE
            if (index.get() > 10 && index.get() < 20) {
                session.remove(card);//Usunięcie DELETE
            }
        });

        session.flush();//Wysłanie zmian do bazy

        tx.rollback();//Po to aby można było wielokrotnie pracować z tymi samymi danymi

        //close resources
        session.close();
    }
}
