package pl.softwareskill.course.hibernate.persistencecontext;

import lombok.RequiredArgsConstructor;
import org.hibernate.Session;
import pl.softwareskill.course.hibernate.Card;

@RequiredArgsConstructor
class NativePersistenceContextCardRepository {

    private final Session session;

    public Card findById(String cardId) {

        var tx = session.beginTransaction();

        Card card = session.get(Card.class, cardId);

//        tx.rollback();//rollback i commit czyści PC

//        card = session.get(Card.class, cardId);//get po rollback/commit rzuci wyjątek

        return card;
    }
}