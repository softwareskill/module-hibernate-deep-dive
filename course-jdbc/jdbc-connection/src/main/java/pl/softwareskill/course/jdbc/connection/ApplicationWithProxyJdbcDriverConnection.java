package pl.softwareskill.course.jdbc.connection;

import java.sql.DriverManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Aplikacja demonstracyjna
 * <p>
 * Funkcjonalność - połączenie do bazy danych H2 poprzez P6Spy (driver proxy). Dodatkowo skonfigurowane rozdszerzone logowanie
 * przez sterowniok P6Spy
 * <p>
 * Aplikacja nie wymaga podania parametrów
 */
public class ApplicationWithProxyJdbcDriverConnection {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationWithProxyJdbcDriverConnection.class);

    public static void main(String[] args) throws Exception {
        //Kod demonstracyjny

        //rejestracja klasy sterownika
        //Nie jest konieczne bo jest plik META-INF/services/java.sqlDriver w jar ze sterownikiem
        Class.forName("com.p6spy.engine.spy.P6SpyDriver");

        //nawiązanie połączenia do bazy H2 z wykorzystaniem drivera proxy P6Spy
        var connection = DriverManager.getConnection(
                "jdbc:p6spy:h2:mem:softwareskill_orm;TRACE_LEVEL_SYSTEM_OUT=3", //TRACE_LEVEL_SYSTEM_OUT wartość z zakresu 1|2|3
                "ormowner",
                "");

        //nawiązanie połączenia do bazy PostgreSQL z wykorzystaniem drivera proxy P6Spy
//        var connection = DriverManager.getConnection(
//                "jdbc:p6spy:postgresql:softwareskill?loggerLevel=debug",
//                "softwareskill",
//                "softwareskill");

        connection.createStatement().executeQuery("Select 1 ");
        LOGGER.info("Połączono do bazy danych z wykorzystaniem połączenia JDBC");
    }
}
