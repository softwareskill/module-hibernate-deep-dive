# README #

Aplikacje demo SoftwareSkill dla modułu obsługi baz danych - część dotycząca podstawowej obsługi baz danych w Java z
wykorzystaniem JDBC API.

### Po co to repozytorium? ###

Repozytorium prezentuje sposób konfiguracji i wykorzystania JDBC API. 

* Wersja 1.0

### Opis ###

* Funkcjonalność - aplikacje w ramach modułów realizują logikę dla prezentowania możliwości JDBC API. W nagłówku javadoc klas znajduje się opis oraz sosób uruchomienia. 
* Baza danych - baza danych PostgreSQL lub H2. W strukturze znajdują się odpiwiednie skrypty SQL dla stworzenia tabel w bazie danych. W przypadku PostgreSQL użyhtkownik i baza muszą być utworzone i muszą być nadane odpowiednie uprawnienia.
* Konfiguracja - w zależności od aplikacji, dodatkowo konfiguracja logowania (logback, p6spy) - zobacz pliki w resources.
* Zależności - H2 (baza danych in memory), PostgreSQL, Logback (framework do logów aplikacyjnych), Lombok, Hikari jako dostawca puli połączeń, P6Spy driver proxy dla JDBC
* Jak uruchomić aplikację - z linii poleceń klasy, mając w nazwie Appliaction z odpowiednimi parametrami (opis w nagłówku klasy z Applikation w nazwie).

### Z kim się kontaktować? ###

* Właściciel repozytorium kodu - SoftwareSkill
* Autorzy rozwiązania - Krzysztof Kądziołka krzysztof.kadziolka@gmail.com