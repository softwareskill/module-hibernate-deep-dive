package pl.softwareskill.course.jdbc.statements;

import java.sql.BatchUpdateException;

/**
 * Kod demonstracyjny
 *
 * Aplikacja prezentuje tryb wsadowy/batch  dla JDBC - dwa poprawne polecenia UPDATE oraz DELETE + operacja, która spowoduje błąd
 * po stronie bazy danych (DELETE z nieistniejącej tabeli).
 *
 * Zostanie rzucony wyjątek BatchUpdateException - pokazanie zawartości wyjątku dla sterownika PostgreSQLs
 *
 * Uruchomienie nie wymaga podawania parametrów
 */
public class StatementFailingBatchExampleApplication {

    public static void main(String[] args) throws Exception {
        //Nawiązanie połączenia (PostgreSQL + Hikari, parametry w pliku datasource.properties)
        var connection = HikariDatasourceConnection.getConnection();

        try (var statement = connection.createStatement()) {
            //Zapytanie SQL
            String sqlQuery1 = "UPDATE ORDERS SET AMOUNT_TO_PAY=12.67 WHERE ORDER_NUMBER='1'";
            statement.addBatch(sqlQuery1);

            String sqlQuery2 = "DELETE FROM ORDERS WHERE ORDER_NUMBER='11111'";
            statement.addBatch(sqlQuery2);

            String sqlQuery3 = "DELETE FROM NOT_EXISTING_TABLE";
            statement.addBatch(sqlQuery3);

            //Wykonanie zapytania z pobraniem liczby zmodyfikowanych wierszy
            var result = statement.executeBatch();
        } catch (BatchUpdateException e) {
            System.err.println("Błąd dla polecenia " + e.getLocalizedMessage());
            int[] batchStatuses = e.getUpdateCounts();
            for (int i = 0; i < batchStatuses.length; i++) {

                //W zależności od sterownika JDBC getUpdateCounts może zwrócić dla wszystkich
                //elementów -3 == Statement.EXECUTE_FAILED
                System.err.println("Status polecenia " + i + " wynosi " + batchStatuses[i]);
            }
            e.printStackTrace();
        }

        //Zwolnienie połączenia
        connection.close();

        //Zwolnienie puli
        HikariDatasourceConnection.close();
    }
}
