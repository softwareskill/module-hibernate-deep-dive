CREATE TABLE CARDS (
    CARD_ID VARCHAR(20) NOT NULL PRIMARY KEY,
    CARD_UUID VARCHAR(40) NOT NULL,
    ENABLED VARCHAR(1) NOT NULL,
    COUNTRY VARCHAR(2) NOT NULL,
    CARD_BALANCE DECIMAL(10,2) NOT NULL
);

CREATE TABLE CUSTOMERS (
    CUSTOMER_ID VARCHAR(20) NOT NULL PRIMARY KEY,
    NAME VARCHAR(255) NOT NULL,
    EMAIL_ADDRESS VARCHAR(40) NOT NULL,
    CARD_ID VARCHAR(20) NOT NULL,
    FOREIGN KEY (CARD_ID) REFERENCES CARDS(CARD_ID)
);

CREATE INDEX IDX_CUSTOMER_CARDS ON CUSTOMERS(CARD_ID);

CREATE TABLE ORDERS (
    ORDER_NUMBER VARCHAR(20) NOT NULL PRIMARY KEY,
    AMOUNT_TO_PAY DECIMAL(22,2) NOT NULL,
    ORDER_STATUS VARCHAR(30) NOT NULL
);

CREATE TABLE BIGTABLE (
    ID VARCHAR(20) NOT NULL PRIMARY KEY,
    CONTENT TEXT
);


INSERT INTO BIGTABLE(ID,CONTENT) VALUES ('1','AAAAAAAAAABBBBBBBBBBCCCCCCCCCCCDDDDDDDDDDEEEEEEEEEEFFFFFFFFFFGGGGGGGGGG');

INSERT INTO CARDS(CARD_ID,CARD_UUID,ENABLED,COUNTRY,CARD_BALANCE) VALUES ('1', '8861e09d-6d6e-47c3-ba06-18dc6374a939', 'Y', 'PL',100);
INSERT INTO CARDS(CARD_ID,CARD_UUID,ENABLED,COUNTRY,CARD_BALANCE) VALUES ('2', '8861e09d-6d6e-47c3-ba06-18dc6374a888', 'Y', 'PL',50);

INSERT INTO CUSTOMERS(CUSTOMER_ID,NAME,EMAIL_ADDRESS,CARD_ID) VALUES ('1', 'Krzysztof Kadziolka', 'krzysztof.kadziolka@gmail.com','1');
INSERT INTO CUSTOMERS(CUSTOMER_ID,NAME,EMAIL_ADDRESS,CARD_ID) VALUES ('2', 'Bob Sapp', 'bsxdrt@gmail.com','2');

INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('1', 100, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('2', 300, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('3', 120, 'PAID');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('4', 120, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('5', 120, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('6', 120, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('7', 120, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('8', 120, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('9', 120, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('10', 120, 'WAITING_FOR_PAYMENT');
INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES ('11', 120, 'WAITING_FOR_PAYMENT');


CREATE OR REPLACE PROCEDURE createNewOrder(IN cardId varchar, IN amount REAL, INOUT status VARCHAR)
/**
* amount musi byc REAL gdyz inaczej zarejestrue sie jako numeric i nie zostanie odnaleziona ustawiajac w java setFloat
*/
AS $$
BEGIN
    status := 'WAITING_FOR_PAYMENT';
    INSERT INTO ORDERS(ORDER_NUMBER,AMOUNT_TO_PAY,ORDER_STATUS) VALUES (cardId, amount, status);
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION subtractFromCard(IN cardId varchar, IN amount REAL)
RETURNS REAL
AS $$
DECLARE FINAL_BALANCE DECIMAL;
BEGIN
    UPDATE CARDS SET CARD_BALANCE=CARD_BALANCE-amount WHERE CARD_ID=cardId;
    SELECT CARD_BALANCE INTO FINAL_BALANCE FROM CARDS WHERE CARD_ID=cardId;
	RETURN FINAL_BALANCE;
END;
$$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE TYPE address AS (
    street varchar(100),
    postalCode varchar(10)
);

CREATE TABLE PERSON (
    living_address address,
    name varchar(100)
);

INSERT INTO PERSON VALUES (ROW('street', '42-300'), 'Krzysztof Kadziolka');